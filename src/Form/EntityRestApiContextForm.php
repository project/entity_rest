<?php

namespace Drupal\entity_rest\Form;

use Drupal\Core\Form\FormStateInterface;

/**
 * Entity rest context form.
 */
class EntityRestApiContextForm extends EntityRestApiForm {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {

    return 'entity_rest_api_context_form';
  }

  /**
   * Returns an array containing the table headers.
   *
   * @return array
   *   The table header.
   */
  protected function getTableHeader(): array {
    $header = parent::getTableHeader();
    $header['allowed_methods'] = $this->t(
      'Allowed methods for <strong>%context</strong> context',
      ['%context' => $this->getContext()]
    );

    return $header;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {

    $form = parent::buildForm($form, $form_state);

    foreach ($this->getFields() as $field) {
      $method_options = [];
      $method_default = [];
      foreach ($this->entityRestService->getMethods() as $method => $method_name) {
        $method_options[$method] = $method_name;
        if ($this->getConfig()->get('methods.' . $this->getContext() . '.' . $method . '.' . $field->getName())) {
          $method_default[] = $method;
        }
      }
      $form['field_list'][$field->getName()]['methods'] = [
        '#type' => 'checkboxes',
        '#options' => $method_options,
        '#default_value' => $method_default,
      ];
    }

    return $form;
  }

  /**
   * @inerhitDoc
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * @inheritDoc
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $values = $form_state->getValues();
    $methods = [];
    foreach ($values['field_list'] as $field_name => $field_values) {
      foreach ($field_values['methods'] as $method => $enabled) {
        if ($enabled) {
          $methods[$values['context']][$method][$field_name] = 1;
        }
      }
    }

    $editable_config = $this->configFactory->getEditable('entity_rest.api.' . $values['entity_type_id'] . '.' . $values['bundle']);
    $existing_methods = $editable_config->get('methods') ?? [];
    $editable_config->set('methods', $methods ? $methods + $existing_methods : $methods)->save();

    $this->messenger()->addMessage($this->t('The API configuration for %bundle has been saved', ['%bundle' => $values['bundle']]));
  }

}
