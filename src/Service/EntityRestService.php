<?php

namespace Drupal\entity_rest\Service;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\user\EntityOwnerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class EntityRestService {

  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The Configuration Factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $currentRequest;

  /**
   * The current user account.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * EntityRestService constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The current request.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The current user account.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    ConfigFactoryInterface $config_factory,
    RequestStack $request_stack,
    AccountInterface $account
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->configFactory = $config_factory;
    $this->currentRequest = $request_stack->getCurrentRequest();
    $this->currentUser = $account;
  }

  /**
   * Returns an array containing the HTTP methods.
   *
   * @return array
   *   The methods.
   */
  public function getMethods(): array {
    return [
      'get' => 'GET',
      'post' => 'POST',
      'patch' => 'PATCH',
    ];
  }

  /**
   * Get API contexts with their labels.
   *
   * @return array
   *   The contexts label, keyed by machine name.
   */
  public function getApiContexts(): array {
    return [
      'own' => $this->t('Own'),
      'all' => $this->t('All'),
      'reference' => $this->t('Reference'),
    ];
  }

  /**
   * Returns the config of a given entity API name.
   *
   * @param $entity_api_name
   *
   * @return \Drupal\Core\Config\ImmutableConfig|null
   */
  public function getEntityApiConfig($entity_api_name): ?ImmutableConfig {
    $prefix = 'entity_rest.api.';
    foreach ($this->configFactory->listAll($prefix) as $config_name) {
      $params = explode('.', substr($config_name, strlen($prefix)));
      $entity_type_id = $params[0];
      $bundle = $params[1];
      $config = $this->configFactory->get($prefix . $entity_type_id . '.' . $bundle);
      if ($config->get('entity_api_name') == $entity_api_name) {
        return $config;
      }
    }

    return null;
  }

  /**
   * Request entities in the database.
   *
   * @param $entity_type_id
   *   The entity type id to request.
   *
   * @return array|int
   *   The query executed.
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getEntities($entity_type_id) {
    $storage = $this->entityTypeManager->getStorage($entity_type_id);
    $query = $storage->getQuery();
    //$query->condition('status', 1);
    $this->handleQueryParameters($query);

    return $query->execute();
  }

  /**
   * Get the fields of an entity for the API.
   *
   * @param $config
   *   The configuration.
   * @param $entity
   *   The entity.
   * @param $method
   *   The HTTP method.
   *
   * @return array
   *   The fields and their values.
   */
  public function getEntityFields($config, $entity, $method): array {
    if ($config_methods = $config->get('methods')) {
      $field_api_names = $config->get('field_api_names');
      if ($entity->access('view', $this->currentUser)) {
        $owner_uid = $this->getEntityOwner($entity);
        if (array_key_exists('all', $config_methods)) {
          foreach (array_keys($config_methods['all'][$method]) as $field_name) {
            $fields[$field_api_names[$field_name]] = $this->getFieldValue($entity, $field_name);
          }
        }
        if (array_key_exists('own', $config_methods)) {
          if ($this->currentUser->id() == $owner_uid) {
            foreach (array_keys($config_methods['own'][$method]) as $field_name) {
              $fields[$field_api_names[$field_name]] = $this->getFieldValue($entity, $field_name);
            }
          }
        }
      }
    }

    return $fields ?? [];
  }

  /**
   * Get the value of a field from an entity.
   *
   * @param $entity
   *   The Entity containing the field.
   * @param $field_name
   *   The field name.
   *
   * @return mixed
   *   The value.
   */
  public function getFieldValue($entity, $field_name) {

    return $entity->get($field_name)->getValue();
  }

  /**
   * Get the owner of an entity.
   * Is there a better way to do it?!
   *
   * @param $entity
   *   The entity.
   *
   * @return int|null
   *   The id of the owner or null if no owner.
   */
  public function getEntityOwner($entity): ?int {
    if ($entity instanceof EntityOwnerInterface) {
      return $entity->getOwnerId();
    }
    if ($entity->hasField('uid')) {
      if ($owner = $entity->get('uid')->first()->getValue()) {
        return $owner['target_id'] ?? $owner['value'] ?? null;
      }
    }

    return null;
  }

  /**
   * Specify the default query parameter values for a content request.
   *
   * @return array
   *   The values keyed by the query parameter name.
   */
  public function getDefaultQueryParameters(): array {

    return [
      'offset' => 0,
      'limit' => 10,
      'sort' => 'uuid',
      'order' => 'DESC',
    ];
  }

  /**
   * Take query parameters from URL and use them in a database query.
   *
   * @param \Drupal\Core\Entity\Query\QueryInterface $query
   *   The Query.
   */
  public function handleQueryParameters(QueryInterface &$query) {
    $defaults = $this->getDefaultQueryParameters();
    $offset = $this->currentRequest->get('offset', $defaults['offset']);
    $limit = $this->currentRequest->get('limit', $defaults['limit']);
    $sort = $this->currentRequest->get('sort', $defaults['sort']);
    $direction = $this->currentRequest->get('order', $defaults['order']);

    $query->range($offset, $limit);
    $query->sort($sort, $direction);
  }

}
